package net.ciuak.gam.main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GAMKeyListener implements KeyListener {

    @Override
    public void keyPressed(KeyEvent arg0) {
        GAMSession.addKEvent(arg0);
        GAMSession.escCheck();
    }

    @Override
    public void keyReleased(KeyEvent arg0) {
        GAMSession.removeKEvent(arg0);
        GAMSession.escCheck();
    }

    @Override
    public void keyTyped(KeyEvent arg0) {

    }


}
