package net.ciuak.gam.main;

import java.awt.*;
import java.awt.image.BufferedImage;

@SuppressWarnings("serial")
public class GAMCanvas extends Canvas {

	int pos = 0;
	long fps = 0;
	long last = 0;
	BufferedImage offscr;
	Graphics offscrg;

	public GAMCanvas() {
		this.setSize(640, 640);
		initializeOffscreen();
	}

	private void initializeOffscreen() {
		offscr = new BufferedImage(this.getWidth(), this.getHeight(),
				BufferedImage.TYPE_INT_RGB);
		offscrg = offscr.getGraphics();
	}

	public void renderGame() {
        offscrg.setFont(Font.decode("Sans-Serif 48"));
        offscrg.setColor(new Color(255, 255, 255, 65));
		// offscrg.drawString(""+fps, this.getWidth()/2, this.getHeight()/2);
		offscrg.setColor(new Color(255, 255, 255, 65));
		offscrg.drawRect(5, 5, getWidth() - 10, getHeight() - 10);
		for (Entity e : GAMSession.getEntities()) {
			if(e != null) {
				e.render(offscrg);
			}
		}
		offscrg.setColor(new Color(255, 255, 255, 60));
		offscrg.drawOval(getWidth() / 2 - 100, getHeight() / 2 - 100, 200, 200);
		int edgecount = GAMSession.lives;
        if (GAMSession.lives > 2) {
            offscrg.setColor(new Color(255, 255, 255));
			int rx = (int) (getWidth() / 2.0 + 100 * Math
					.sin(GAMSession.pangle));
			int ry = (int) (getHeight() / 2.0 + 100 * Math
					.cos(GAMSession.pangle));
			offscrg.fillRect(rx - 1, ry - 1, 3, 3);
			int[][] points = new int[2][edgecount];
			for (int i = 0; i < edgecount; ++i) {
				double ang = GAMSession.angle + ((double) i) / edgecount
						* Math.toRadians(360);
				points[0][i] = (int) (getWidth() / 2.0 + (30 * Math.cos(ang)));
				points[1][i] = (int) (getHeight() / 2.0 + (30 * Math.sin(ang)));
			}
			offscrg.setColor(Color.getHSBColor((float) GAMSession.col, 1F, 1F));
			offscrg.fillPolygon(points[0], points[1], edgecount);
			offscrg.setColor(Color.WHITE);
			for (int i = 0; i < edgecount; ++i) {
				offscrg.fillRect(points[0][i], points[1][i], 1, 1);
			}
            offscrg.setFont(Font.decode("Sans-Serif 24"));
            offscrg.setColor(new Color(255, 255, 255));
            int w = getWidth() / 2 - offscrg.getFontMetrics().stringWidth(GAMSession.lives + "") / 2;
            int h = getHeight() / 2 + offscrg.getFontMetrics().getHeight() / 4;
            offscrg.drawString(GAMSession.lives + "", w, h);
            w = getWidth() - offscrg.getFontMetrics().stringWidth(GAMSession.score + "");
            h = offscrg.getFontMetrics().getHeight();
        }
        int w = getWidth() - offscrg.getFontMetrics().stringWidth(GAMSession.score + "");
        int h = offscrg.getFontMetrics().getHeight();
        if (!GAMSession.isMenu || GAMSession.inGame)
            offscrg.drawString(GAMSession.score + "", w, h);
    }

	public void render() {
		initializeOffscreen();
        renderGame();
        if (!GAMSession.inGame)
            if (GAMSession.isMenu)
                renderMainMenu();
            else
                renderPauseMenu();
        else if (GAMSession.lives < 3)
            renderGameOverMenu();
    }

    public void renderMainMenu() {
        offscrg.setColor(new Color(255, 255, 255, 108));
        offscrg.fillRect(0, 0, getWidth(), getHeight());
        offscrg.setFont(Font.decode("Sans-Serif 36"));
        offscrg.setColor(new Color(255, 255, 255));
        int w = getWidth() / 2 - offscrg.getFontMetrics().stringWidth("Press any key to start") / 2;
        int h = getHeight() / 2 + offscrg.getFontMetrics().getHeight() / 4;
        offscrg.drawString("Press any key to start", w, h);
    }

    public void renderGameOverMenu() {
        offscrg.setColor(new Color(255, 255, 255, 108));
        offscrg.fillRect(0, 0, getWidth(), getHeight());
        offscrg.setFont(Font.decode("Sans-Serif 48"));
        offscrg.setColor(new Color(255, 255, 255));
        int w = getWidth() / 2 - offscrg.getFontMetrics().stringWidth("GAME OVER") / 2;
        int h = getHeight() / 2 + offscrg.getFontMetrics().getHeight() / 4;
        offscrg.drawString("GAME OVER", w, h);
        offscrg.setFont(Font.decode("Sans-Serif 24"));
        w = getWidth() / 2 - offscrg.getFontMetrics().stringWidth("Press any key to restart") / 2;
        h = getHeight() / 2 + 32 + offscrg.getFontMetrics().getHeight() / 4;
        offscrg.drawString("Press any key to restart", w, h);
    }

    public void renderPauseMenu() {
        offscrg.setColor(new Color(255, 255, 255, 108));
        offscrg.fillRect(0, 0, getWidth(), getHeight());
        offscrg.setFont(Font.decode("Sans-Serif 48"));
        offscrg.setColor(new Color(255, 255, 255));
        int w = getWidth() / 2 - offscrg.getFontMetrics().stringWidth("PAUSED") / 2;
        int h = getHeight() / 2 + offscrg.getFontMetrics().getHeight() / 4;
        offscrg.drawString("PAUSED", w, h);
    }

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.BLACK);
		// g.fillRect(0, 0, getWidth(), getHeight());
		render();
		g.drawImage(offscr, 0, 0, offscr.getWidth(), offscr.getHeight(), 0, 0,
				offscr.getWidth(), offscr.getHeight(), GAMSession.frame);
	}

	@Override
	public void update(Graphics g) {
		paint(g);
	}

}
