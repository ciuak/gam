package net.ciuak.gam.main;

public class Ticker extends Thread {

    private final boolean function;
    private final int id;
    private double tickFrequency = 0;
    private long ticks = 0;

    public Ticker(boolean function, double tickFrequency) {
        id = (int) (Math.random() * Integer.MAX_VALUE);
        this.function = function;
        this.tickFrequency = tickFrequency;
        this.setName(toString());
    }

    public double getTickFrequency() {
        return tickFrequency;
    }

    public long getTicks() {
        return ticks;
    }

    @Override
    public String toString() {
        return "TickerThread[id=" + id + ",job=" + function + ",freq="
                + tickFrequency + "]";
    }

    @Override
    public void run() {
        while (true)
            if (tickFrequency > 0) {
                double tf = tickFrequency;
                double time = 1000 / tf;
                long millis = (long) time;
                long timestamp = System.currentTimeMillis();
                if (function)
                    GAMSession.tick();
                else
                    GAMSession.gamcanvas.repaint();
                ticks++;
                while (System.currentTimeMillis() < timestamp + millis) ;
            }
    }

    public void resetTicks() {
        ticks = 0;
    }
}
