package net.ciuak.gam.main;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;


public abstract class GAMSession {

    private static final int initialLives = 24;
    public static boolean isOn;
    public static boolean inGame;
    public static boolean isMenu;
    public static long score = 0;
    public static Frame frame;
    public static GAMCanvas gamcanvas;
    public static Ticker entityticker;
	public static Ticker qcticker;
	public static MouseEvent mousePress;
	public static MouseEvent lastMouseMovement;
	public static double pangle = 0;
    public static int lives = 0;
    public static double angle = 0;
    public static double col = 0;
    public static KeyEvent[] keyEvents = {};
    public static boolean escKeyPressed = false;
    private static Entity[] entities = {};
    private static KeyEvent[] keyEventsS;
    private static KeyEvent[] oldKeyEvents = {};

    public static void log(WarningLevel level, String id, String message) {
        System.out.println(
                "[ " + level.letter + " "
                        + DateFormat.getTimeInstance(DateFormat.MEDIUM).format(new Date(System.currentTimeMillis()))
                        + " | " + id + " ] " + message
        );
	}

    public static void init(Frame f, GAMCanvas qc, Ticker et, Ticker qct) {
        inGame = false;
        isMenu = true;
        frame = f;
        gamcanvas = qc;
        gamcanvas.setVisible(true);
        gamcanvas.addMouseListener(new GAMMouseListener());
        gamcanvas.addMouseMotionListener(new GAMMouseMotionListener());
        gamcanvas.addKeyListener(new GAMKeyListener());
        f.add(qc);
        f.setTitle("GAM");
        f.pack();
        f.setVisible(true);
        entityticker = et;
        qcticker = qct;
        qcticker.start();
        entityticker.start();
        isOn = true;
    }

    public static void tick() {
        if (inGame) {
            int[] screencenter = {gamcanvas.getWidth() / 2,
                    gamcanvas.getHeight() / 2};
            try {
                int x = lastMouseMovement.getX() - screencenter[0];
                int y = lastMouseMovement.getY() - screencenter[1];
                pangle = Math.atan2(x, y);
            } catch (NullPointerException e) {
                pangle = 0;
            }
            for (int i = 0; i < entities.length; i++) {
                Entity e = entities[i];
                for (Entity f : Arrays.copyOfRange(entities, i + 1, entities.length)) {
                    try {
                        if (e.getEdgeCount() < 2 || f.getEdgeCount() < 2) continue;
                        double rx = e.x - f.x;
                        double ry = e.y - f.y;
                        rx *= rx;
                        ry *= ry;
                        /* if (rx + ry < 30 * 30) {
                            Vector temp = f.speed;
                            f.speed = e.speed;
                            e.speed = temp;
                        } */
                        double direction = Math.atan2(e.y - f.y, e.x - f.x);
                        double force = 5000d / (rx + ry);
                        final double maxforce = 2;
                        if (force > maxforce) force = maxforce;
                        Vector eForce = new Vector(force, Math.PI + direction, true);
                        Vector fForce = new Vector(force, direction, true);
                        e.addSpeed(eForce);
                        f.addSpeed(fForce);
                    } catch (NullPointerException ignore) {
                    }
                }
                try {
                    e.tickUpdate();
                } catch (NullPointerException ignore) {
                }
            }
            angle += 1D / Math.pow(lives, 2);
            col += lives / 1000D;
            col = col - (int) col;
            if (lives < 3 && keyEvents.length > 0) {
                lives = initialLives;
                score = 0;
                entities = new Entity[0];
                entityticker.resetTicks();
            }
        } else if (isMenu) {
            if (keyEvents.length > 0) {
                inGame = true;
                isMenu = false;
                score = 0;
                lives = initialLives;
                entities = new Entity[0];
                entityticker.resetTicks();
            }
        }
        if (!Arrays.deepEquals(keyEvents, oldKeyEvents)) {
            log(WarningLevel.LV_DEBUG, "GAMSession", "Pressed keys: " + toStringKEvents(true));
            oldKeyEvents = keyEvents.clone();
        }
        if (entityticker.getTicks() % entityticker.getTickFrequency() == 0) refreshEntities();
    }

//	public static void init() {
//		isOn = true;
//	}
//
//	public static void deinit() {
//		isOn = false;
//	}

    public static void registerEntity(Entity e) {
        Entity[] entitiesS = new Entity[entities.length + 1];
        System.arraycopy(entities, 0, entitiesS, 0, entities.length);
        entitiesS[entities.length] = e;
        entities = entitiesS;
    }

    public static void escCheck() {
        boolean isEscPressed = GAMSession.escKeyPressed;
        try {
            isEscPressed = new LinkedList<>(Arrays.asList(GAMSession.keyEvents)).removeIf((KeyEvent ke) -> ke.getKeyCode() == 27);
        } catch(NullPointerException ignore) {}
        if (isEscPressed && !GAMSession.escKeyPressed && !GAMSession.isMenu) {
            GAMSession.inGame = !GAMSession.inGame;
        }
        GAMSession.escKeyPressed = isEscPressed;
    }

    private static void refreshEntities() {
        Entity[] entitiesS;
        int no = 0;
        for (Entity entity : entities) {
            if (entity != null && entity.getEdgeCount() >= 2)
                no++;
        }
        entitiesS = new Entity[no];
        for (int i = 0, j = 0; i < entities.length; ++i) {
            if (entities[i] != null && entities[i].getEdgeCount() >= 2) {
                entitiesS[j] = entities[i];
                j++;
            }
        }
        if (entities.length > entitiesS.length) {
            GAMSession.log(WarningLevel.LV_DEBUG, "GAMSession Entity Cleanup", "Found " + (entities.length - entitiesS.length) + " dead entities");
        }
        entities = entitiesS;
    }

    public static void addKEvent(KeyEvent arg0) {
        keyEventsS = new KeyEvent[keyEvents.length + 1];
        System.arraycopy(keyEvents, 0, keyEventsS, 0, keyEvents.length);
        keyEventsS[keyEvents.length] = arg0;
        keyEvents = keyEventsS;
    }

    public static void removeKEvent(KeyEvent arg0) {
        keyEventsS = new KeyEvent[keyEvents.length - 1];
        for (int i = 0, j = 0; i < keyEventsS.length; ++i) {
            if (arg0.equals(keyEvents[i])) {
                j++;
                continue;
            }
            keyEventsS[j] = keyEvents[i];
        }
        keyEvents = keyEventsS;
    }

    public static String toStringKEvents(boolean type) {
        String retour = "[";
        for (int i = 0; i < keyEvents.length; ++i) {
            try {
                if (type && (keyEvents[i].getKeyCode() >= 32)
                        && (keyEvents[i].getKeyCode() <= 126)) {
                    retour += "'" + (char) keyEvents[i].getKeyCode() + "'"
                            + (i < keyEvents.length - 1 ? ", " : "");
                } else {
                    retour += Integer.toHexString(keyEvents[i].getKeyCode())
                            .toUpperCase()
                            + (i < keyEvents.length - 1 ? ", " : "");
                }
            } catch (NullPointerException | ArrayIndexOutOfBoundsException ignore) {
            }
        }
        return retour + "]";
    }

    public static Entity[] getEntities() {
        return entities;
    }

    public static void addScore(long score) {
        GAMSession.score += (int) (score * Math.max(1, Math.sqrt(entityticker.getTicks() / 4000d)));
    }

    public static enum WarningLevel {
        LV_VERBOSE('V'), LV_DEBUG('D'), LV_NONE(' '), LV_INFO('I'), LV_WARN('W'), LV_ERROR('E'), LV_CRASH('C');
        public char letter;
        WarningLevel(char letter) {
            this.letter = letter;
        }
    }

}