package net.ciuak.gam.main;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class GAMWindowListener implements WindowListener {

	public void windowActivated(WindowEvent arg0) {
        if (!GAMSession.isMenu)
            GAMSession.inGame = true;
    }

	public void windowClosed(WindowEvent arg0) {
		
	}

	public void windowClosing(WindowEvent arg0) {
        GAMSession.log(GAMSession.WarningLevel.LV_INFO, "Meta", "Stopping");
        Runtime.getRuntime().exit(0);
    }

	public void windowDeactivated(WindowEvent arg0) {
        GAMSession.inGame = false;
    }

	public void windowDeiconified(WindowEvent arg0) {

	}

	public void windowIconified(WindowEvent arg0) {

	}

	public void windowOpened(WindowEvent arg0) {

	}

}
