package net.ciuak.gam.main;

public class Vector {

    public double x;
    public double y;
    public String ID;

    public Vector(double a, double b) {
        x = a;
        y = b;
    }

    public Vector() {
        this(0, 0);
    }

    public Vector(double len, double arg, boolean radians) {
        double rarg = arg * (radians ? 1 : Math.PI / 180);
        x = Math.cos(rarg) * len;
        y = Math.sin(rarg) * len;
    }

    public double arg(boolean radians) {
        return Math.atan2(y, x) * (radians ? 1 : 180 / Math.PI);
    }

    public double len() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    public Vector uni() {
        return new Vector(x / len(), y / len());
    }

}
