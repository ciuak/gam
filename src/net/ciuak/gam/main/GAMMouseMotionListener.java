package net.ciuak.gam.main;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class GAMMouseMotionListener implements MouseMotionListener {

    @Override
    public void mouseDragged(MouseEvent arg0) {
        GAMSession.lastMouseMovement = arg0;
    }

    @Override
    public void mouseMoved(MouseEvent arg0) {
        GAMSession.lastMouseMovement = arg0;
    }

}
