package net.ciuak.gam.main;

import java.awt.*;

public class MainThread extends Thread {

	public MainThread() {
		super.start();
	}

	@Override
	public void run() {
		GAMCanvas qc = new GAMCanvas();
		Frame f = new Frame();
		f.setResizable(false);
		f.addWindowListener(new GAMWindowListener());
		Ticker t = new Ticker(true, 80);
		Ticker t2 = new Ticker(false, Double.POSITIVE_INFINITY);
		GAMSession.init(f, qc, t, t2);
        int i = 0;
        while (true) {
            try {
                while (!GAMSession.inGame || GAMSession.isMenu || GAMSession.lives < 3) {
                    if (GAMSession.lives < 3) i = 0;
                    Thread.sleep(100);
                }
                Thread.sleep((long) ((Math.min(4000000d / (GAMSession.entityticker.getTicks() + 1), 1000)
                        + Math.min(4000000d / (GAMSession.entityticker.getTicks() + 1), 1000) * Math.random())));
                Vector hv = new Vector(700D, Math.random() * 360, false);
                Entity entity;
                if (Math.random() < 0.8) {
                    entity = new Entity(hv.y + qc.getWidth() / 2D, hv.x
                            + qc.getHeight() / 2D, (int) (Math.random() * 5 + 5));
                } else {
                    entity = new Entity(hv.y + qc.getWidth() / 2D, hv.x
                            + qc.getHeight() / 2D, (int) (Math.random() * 5 + 3), Entity.EntityType.BONUS);
                }
                entity.addSpeed(new Vector(80.0D + 40.0D * Math.random(),
                        hv.arg(false) + 180, false));
                GAMSession.registerEntity(entity);
                GAMSession.log(GAMSession.WarningLevel.LV_DEBUG, "Spawning Thread", "Spawned Entity[i=" + i + "]");
                i++;
            } catch (NullPointerException | InterruptedException ignore) {
            }
        }
    }

}
