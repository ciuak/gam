package net.ciuak.gam.main;

import java.awt.*;

public class Entity {

    public Vector speed;
    public double x;
    public double y;
    private Vector[] forces;
    private int edgecount;
    private double momentum;
    private double angle;
    // private double mcolor = 0;
    private boolean ready;
    private boolean invulnerable = true;
    private EntityType type;
    private Object[] properties;

    public Entity(double x, double y, int edgecount) {
        this(x, y, edgecount, EntityType.REGULAR);
    }

    public Entity(double x, double y, int edgecount, EntityType type, Object... properties) {
        this.x = x;
        this.y = y;
        this.edgecount = edgecount;
        this.type = type;
        this.properties = properties;
        speed = new Vector();
        forces = new Vector[0];
        angle = Math.random() * Math.PI * 2;
        momentumChange();
        ready = true;
    }

    public Object[] getProperties() {
        return properties;
    }

    public void tickUpdate() {
        if (!ready)
            return;
        if (GAMSession.entityticker.getTickFrequency() > 0) {
            for (Vector force : forces) {
                addSpeed(new Vector(force.x
                        / GAMSession.entityticker.getTickFrequency(), force.y
                        / GAMSession.entityticker.getTickFrequency()));
            }
            angle += momentum / GAMSession.entityticker.getTickFrequency();
        }
        speedAffect();
        wallCollisionsAffect();
        if (GAMSession.lives >= 3) {
            centerCollisionsAffect();
            double w = GAMSession.gamcanvas.getWidth();
            double h = GAMSession.gamcanvas.getHeight();
            Vector move = new Vector(2500000d / ((x - w / 2) * (x - w / 2) + (y - h / 2) * (y - h / 2)),
                    Math.PI + Math.atan2(y - h / 2, x - w / 2), true);
            speed.x += move.x / GAMSession.entityticker.getTickFrequency();
            speed.y += move.y / GAMSession.entityticker.getTickFrequency();
        }
    }

    private void momentumChange() {
        momentum = Math.random() * 12 - 6;
    }

    private void wallCollisionsAffect() {
        int width = GAMSession.gamcanvas.getWidth();
        int height = GAMSession.gamcanvas.getHeight();
        if (invulnerable) {
            if (x > 20 || x < width - 20 || y > 20 || y < height - 20) {
                invulnerable = false;
            }
            return;
        }
        final double feedback = 0.9;
        if (x <= 20) {
            x = 20;
            speed.x = -feedback * speed.x;
            momentumChange();
            decrementEdgecount();
        }
        if (x >= width - 20) {
            x = width - 20;
            speed.x = -feedback * speed.x;
            momentumChange();
            decrementEdgecount();
        }
        if (y <= 20) {
            y = 20;
            speed.y = -feedback * speed.y;
            momentumChange();
            decrementEdgecount();
        }
        if (y >= height - 20) {
            y = height - 20;
            speed.y = -feedback * speed.y;
            momentumChange();
            decrementEdgecount();
        }
    }

    private void decrementEdgecount() {
        if (GAMSession.inGame && GAMSession.lives > 2) {
            edgecount--;
            GAMSession.addScore(1);
        }
    }

    private void centerCollisionsAffect() {
        int width = GAMSession.gamcanvas.getWidth();
        int height = GAMSession.gamcanvas.getHeight();
        double rx = x - (width / 2.0 + 100 * Math.sin(GAMSession.pangle));
        double ry = y - (height / 2.0 + 100 * Math.cos(GAMSession.pangle));
        double rx_sq = rx * rx;
        double ry_sq = ry * ry;
        if (Math.sqrt(rx_sq + ry_sq) <= 15) {
            double rangle = Math.atan2(ry, rx);
            double radius = 15;
            Vector hvector = new Vector(radius, rangle, true);
            rx = hvector.x;
            ry = hvector.y;
            x = rx + (width / 2.0 + 100 * Math.sin(GAMSession.pangle));
            y = ry + (height / 2.0 + 100 * Math.cos(GAMSession.pangle));
            double bounce = rangle - Math.PI / 2;
            double bangle = ((speed.arg(true)) - bounce) % (Math.PI * 2);
            speed = new Vector(speed.len(), speed.arg(true) - 2 * bangle,
                    true);
        }
        rx = x - width / 2D;
        ry = y - width / 2D;
        rx_sq = rx * rx;
        ry_sq = ry * ry;
        if (Math.sqrt(rx_sq + ry_sq) <= 45) {
            double rangle = Math.atan2(ry, rx);
            double radius = 45;
            Vector hvector = new Vector(radius, rangle, true);
            rx = hvector.x;
            ry = hvector.y;
            x = rx + width / 2.0;
            y = ry + height / 2.0;
            double bounce = rangle - Math.PI / 2;
            double bangle = ((speed.arg(true)) - bounce) % (Math.PI * 2);
            speed = new Vector(speed.len(), speed.arg(true) - 2 * bangle,
                    true);
            if (type == EntityType.BONUS) {
                GAMSession.lives += this.edgecount;
                GAMSession.log(GAMSession.WarningLevel.LV_DEBUG, "Entity@" + Integer.toHexString(hashCode()),
                        "Hit the center polygon and healed it by " + this.edgecount
                                + " (now lives=" + GAMSession.lives + ")");
                GAMSession.addScore(edgecount);
                this.edgecount = 0;
            }
            GAMSession.lives--;
            GAMSession.log(GAMSession.WarningLevel.LV_DEBUG, "Entity@" + Integer.toHexString(hashCode()),
                    "Hit the center polygon (now lives=" + GAMSession.lives + ")");
            if (GAMSession.lives < 3)
                GAMSession.log(GAMSession.WarningLevel.LV_INFO, "Entity@" + Integer.toHexString(hashCode()), "Defeat");
            decrementEdgecount();
        }
    }

    public void addSpeed(Vector v) {
        speed = new Vector(speed.x + v.x, speed.y + v.y);
    }

    public void speedAffect() {
        x += speed.x
                * (GAMSession.entityticker.getTickFrequency() > 0
                ? 1 / GAMSession.entityticker.getTickFrequency()
                : 0);
        y += speed.y
                * (GAMSession.entityticker.getTickFrequency() > 0
                ? 1 / GAMSession.entityticker.getTickFrequency()
                : 0);
    }

    public void render(Graphics g) {
        g.setColor(Color.WHITE);
        g.setFont(Font.decode("Monospace 12"));
        // g.drawString(""+edgecount, (int)x, (int)y);
        if (edgecount >= 2) {
            int[][] points = new int[2][edgecount];
            for (int i = 0; i < edgecount; ++i) {
                double ang = this.angle + ((double) i) / edgecount
                        * Math.toRadians(360);
                points[0][i] = (int) (x + (15 * Math.cos(ang)));
                points[1][i] = (int) (y + (15 * Math.sin(ang)));
            }
            for (int i = 0; i < edgecount; ++i) {
                int px = points[0][i];
                int py = points[1][i];
                int pxn = points[0][(i + 1) % edgecount];
                int pyn = points[1][(i + 1) % edgecount];
                g.setColor(new Color(128, 128, 128));
                if (type == EntityType.BONUS) {
                    g.setColor(Color.getHSBColor(
                            (float) (360.0 * GAMSession.entityticker.getTicks()
                                    / GAMSession.entityticker.getTickFrequency()),
                            1F, 0.5F));
                }
                g.drawLine(px, py, pxn, pyn);
                g.setColor(new Color(255, 255, 255));
                if (type == EntityType.BONUS) {
                    g.setColor(Color.getHSBColor(
                            (float) (360.0 * GAMSession.entityticker.getTicks()
                                    / GAMSession.entityticker.getTickFrequency()),
                            1F, 1F));
                }
                g.fillRect(px, py, 1, 1);
            }
            g.fillRect(points[0][0], points[1][0], 1, 1);
        } /*
        g.setColor(Color.RED);
        g.drawLine((int) x, (int) y, (int) (x + speed.x/2), (int) (y + speed.y/2));
        g.setColor(Color.GREEN);
        int w = GAMSession.gamcanvas.getWidth();
        int h = GAMSession.gamcanvas.getHeight();
        Vector move = new Vector(800000d / ((x - w/2)*(x - w/2) + (y - h/2)*(y - h/2)),
                Math.PI + Math.atan2(y - h / 2, x - w / 2), true);
        g.drawLine((int) x, (int) y, (int) (x + move.x), (int) (y + move.y)); */
    }

    public void addForce(Vector v) {
        Vector[] forcesS = new Vector[forces.length + 1];
        System.arraycopy(forces, 0, forcesS, 0, forces.length);
        forcesS[forces.length] = v;
        forces = forcesS;
    }

    public int getEdgeCount() {
        return this.edgecount;
    }

    public EntityType getType() {
        return type;
    }

    public enum EntityType {
        REGULAR, BONUS
    }

}
