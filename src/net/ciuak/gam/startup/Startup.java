package net.ciuak.gam.startup;

import net.ciuak.gam.main.GAMSession;
import net.ciuak.gam.main.MainThread;

public class Startup {

    public static void main(String[] args) {
        MainThread m = new MainThread();
        GAMSession.log(GAMSession.WarningLevel.LV_INFO, "Meta", "Starting");
        m.setName("GAM main thread");
    }

}
